/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerosprimos;

/**
 *
 * @author Mario
 */
public class NumerosPrimos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NumerosPrimos p1 = new NumerosPrimos();
        System.out.println("Los 10 Primeros Numeros Primos: " + p1.primos(100));
        // TODO code application logic here
    }

    public String primos(int numero) {
        String numPrimos = "";
        int cont = 0;
        for (int x = 1; x <= numero; x++) {
            if (esPrimo(x) && cont == 0) {
                numPrimos = x + "";
                cont++;

            } else if (esPrimo(x) && cont < 10) {
                numPrimos = numPrimos + " ," + x;
                cont++;
            }
        }
        int contador = 2;
        boolean primo = true;
        while ((primo) && (contador != numero)) {
            if (numero % contador == 0) {
                primo = false;
            }
            contador++;
        }
        return numPrimos;
    }

    public static boolean esPrimo(int numero) {
        int contador = 2;
        boolean primo = true;
        while ((primo) && (contador != numero)) {
            if (numero % contador == 0) {
                primo = false;
            }
            contador++;
        }
        return primo;

    }

}
